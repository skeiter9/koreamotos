<?php

class Restfull  {

	protected $uri          = array();
	protected $data_input   = array();
	protected $method       = '';
	protected $model        = '';
	protected $model_method = array();
	
	public function __construct($request, $method, $dataInput ) {
 
		$this->uri = explode('/', rtrim($request, '/'));
		$this->data_input=( count($dataInput)>0 )? $dataInput : array() ;
		$this->model = array_shift($this->uri);//get the first param
		$this->model_method = $this->uri;/* //get the second param ant the rest
		

		if (array_key_exists(0, $this->uri) /*&& !is_numeric($this->uri[0])*) {//evaluate if there is a 3th param o more
			for ($i=0; $i < count($this->uri); $i++) { 
				$this->model_method_extras[$i] = array_shift($this->uri);
			}
		}*/
		switch (  strtoupper($method) ) {
			case 'GET':
				$this->method = $method;
				break;
			case 'DELETE':
				$this->method = $method;
				break;
			case 'POST':
				$this->method = $method;
				break;
			case 'PUT':
				$this->method = $method;
				break;
			case 'OPTIONS': // Fix problem ng-upload-file 
				$this->method = 'POST';
				break;
			default:
				throw new Exception("El método de consulta no está permitido");
				break;
		}
	}

	public function respond(){

		header("Access-Control-Allow-Origin: *");
		header("Access-Control-Allow-Methods: *");
		header('Access-Control-Allow-Headers: X-Requested-With');
		header('Access-Control-Max-Age: 86400');
		header('Content-Type: application/json');

		$localModel=strtolower($this->model);

		if (!file_exists("backend/$localModel.php")) {

			$this->_response(400);
			return array('status'=>false,'message'=>"el modelo: ". strtoupper($localModel) ." no existe");

		}else{

			require_once "backend/$localModel.php";

			$className = ucwords(strtolower($localModel));
			$modelClass = new $className();
			$modelTable = strtolower($className);
			
			return $modelClass->rest($this->method, $modelTable, $this->data_input, $this->model_method );
		
		}
	}

	private function _response( $statusCode = 200) {
		$statusRequestList = array(
		    100 => 'Continue',
		    101 => 'Switching Protocols',
		    200 => 'OK',
		    201 => 'Created',
		    202 => 'Accepted',
		    203 => 'Non-Authoritative Information',
		    204 => 'No Content',
		    205 => 'Reset Content',
		    206 => 'Partial Content',
		    300 => 'Multiple Choices',
		    301 => 'Moved Permanently',
		    302 => 'Found',
		    303 => 'See Other',
		    304 => 'Not Modified',
		    305 => 'Use Proxy',
		    306 => '(Unused)',
		    307 => 'Temporary Redirect',
		    400 => 'Bad Request',
		    401 => 'Unauthorized',
		    402 => 'Payment Required',
		    403 => 'Forbidden',
		    404 => 'Not Found',
		    405 => 'Method Not Allowed',
		    406 => 'Not Acceptable',
		    407 => 'Proxy Authentication Required',
		    408 => 'Request Timeout',
		    409 => 'Conflict',
		    410 => 'Gone',
		    411 => 'Length Required',
		    412 => 'Precondition Failed',
		    413 => 'Request Entity Too Large',
		    414 => 'Request-URI Too Long',
		    415 => 'Unsupported Media Type',
		    416 => 'Requested Range Not Satisfiable',
		    417 => 'Expectation Failed',
		    500 => 'Internal Server Error',
		    501 => 'Not Implemented',
		    502 => 'Bad Gateway',
		    503 => 'Service Unavailable',
		    504 => 'Gateway Timeout',
		    505 => 'HTTP Version Not Supported');
		$statusRequest = ($statusRequestList[$statusCode]) ? $statusRequestList[$statusCode] : $statusRequestList[500];
		header("HTTP/1.1 " . $statusCode . " " . $statusRequest);
	}

}
