<?php   

require_once 'cado.php';  

class Trabajador extends Cado{

	private $table = 'trabajador' ;

	public function rest($method, $table, $dataInput,$methods ){//get put post delete

		if ( $method === 'POST' && count($methods) === 0 ) {
			$sql= 'select p.*,ac.id_asignacion_cargo ,u.id_usuario,u.usuario ,t.id_trabajador,t.estado ,t.fecha_contratacion,t.fecha_despido, c.cargo, ac.id_cargo, c.cargo_estado as cargo_estado ,a.nombre as area ,a.id_area, a.estado as area_estado from trabajador as t inner join persona as p on t.id_persona=p.id_persona inner join asignacion_cargo as ac on t.id_trabajador=ac.id_trabajador inner join (select us.id_usuario, us.usuario from usuario as us) as u on ac.id_usuario = u.id_usuario inner join ( select ca.id_cargo,ca.nombre as cargo, ca.estado as cargo_estado, ca.id_area from cargo as ca ) as c on ac.id_cargo = c.id_cargo inner join area as a on c.id_area=a.id_area';
			return $this->query(array('token'=> $dataInput['token'] , 'table'=>$table,'crud'=>'r' ,'indep'=>array($this->table => array('sql'=> $sql) ) )); 
		}

		if ( $method === 'POST' && count($methods) === 1 && $methods[0]==='vendedores' ) {
			$sql= 'select p.*,ac.id_asignacion_cargo ,t.id_trabajador,t.estado ,t.fecha_contratacion,t.fecha_despido, c.cargo, ac.id_cargo, c.cargo_estado as cargo_estado ,a.nombre as area ,a.id_area, a.estado as area_estado from trabajador as t inner join persona as p on t.id_persona=p.id_persona inner join asignacion_cargo as ac on t.id_trabajador=ac.id_trabajador inner join ( select ca.id_cargo,ca.nombre as cargo, ca.estado as cargo_estado, ca.id_area from cargo as ca ) as c on ac.id_cargo = c.id_cargo inner join area as a on c.id_area=a.id_area where ac.id_cargo = 4';
			return $this->query(array('token'=> $dataInput['token'] , 'table'=>$table,'crud'=>'r' ,'indep'=>array($this->table => array('sql'=> $sql) ) )); 
		}

		if ( $method === 'POST' && count($methods) === 1 && $methods[0]==='new') {

			$dataInput['form'] = json_decode( $dataInput['form'], true );

			foreach ($dataInput['form'] as $key => $value) {
				$dataInput[$key] = $value;
			}
			//return $dataInput ;
			$image= $this->upload_image(  $_FILES['file'] , $this->table  ) ;
			//return $image;
			if ( !$image['status'] ) { return $image; }

			$indep=array();
			$dep=array();
			
			$indep['persona'] = array( 'sql'=>" insert into persona (nombres, photo, apellido_paterno, apellido_materno, dni, direccion, fecha_nacimiento, telefono, email) values ( :nombres, :photo, :apellido_paterno, :apellido_materno, :dni, :direccion, :fecha_nacimiento, :telefono, :email )",
																 'array'=>array( ':nombres'=> $dataInput['persona']['nombres'] ,':photo'=> $image['name']  ,':apellido_paterno'=> $dataInput['persona']['apellido_paterno'] ,':apellido_materno'=> $dataInput['persona']['apellido_materno'] ,':dni'=> $dataInput['persona']['dni'] ,':direccion'=> $dataInput['persona']['direccion'] ,':fecha_nacimiento'=> $dataInput['persona']['fecha_nacimiento'] ,':telefono'=> $dataInput['persona']['telefono'] ,':email'=> $dataInput['persona']['email'] ) );

			$dep['trabajador'] = array('sql'=>'insert into trabajador ( estado, fecha_contratacion , id_persona  ) values ( :estado, :fecha_contratacion , :id_persona  ) ', 
																	 'array'=> array(":estado"=> 1 ,':fecha_contratacion' => $dataInput['asignacion_cargo']['fecha_inicio'] , ":id_persona"=>null ) );
				
			//$dep['usuario'] = array('sql'=>"insert into usuario (usuario, password) values ( :usuario, :password )", 
																//'array'=> array( ':usuario'=> strtolower($dataInput['usuario']['usuario']) , ':password'=> $dataInput['usuario']['password'] ) );

			$dep['pago'] = array('sql'=>'insert into pago (tipo) values( :tipo )', 
														 'array'=> array( ':tipo'=> $dataInput['pago']['id_tipo']) ); 

			if ( isset( $dataInput['planilla'] ) ) {
				$dep['planilla'] = array('sql'=>'insert into planilla ( sueldo ) values (:sueldo)', 
																	 'array'=> array( ':sueldo'=> $dataInput['planilla']['sueldo']) );
			}elseif (  isset( $dataInput['noplanilla'] )  ) {
				$dep['noplanilla'] = array('sql'=>'insert into noplanilla ( sueldo ) values (:sueldo)', 
																		 'array'=> array( ':sueldo'=> $dataInput['noplanilla']['sueldo']) );
			}

			$dep['asignacion_cargo'] = array(
																			'sql'=> 'insert into asignacion_cargo ( fecha_inicio, fecha_fin, id_cargo, id_trabajador, id_usuario, id_pago ) values ( :fecha_inicio, :fecha_fin, :id_cargo, :id_trabajador, :id_usuario, :id_pago )', 
																			'array'=> array( ':fecha_inicio'=> $dataInput['asignacion_cargo']['fecha_inicio'] , ':fecha_fin'=> $dataInput['asignacion_cargo']['fecha_fin'] , ':id_cargo'=> $dataInput['asignacion_cargo']['id_cargo'] , ':id_trabajador'=> null , ':id_usuario'=> null , ':id_pago'=> null  ) 
																			); 


			return $this->query( array('token'=> $dataInput['token'] ,'table'=> $this->table, 'crud'=> 'c' ,'indep'=> $indep , 'dep' => $dep  )); 

		}

	}

	public function upload_image ($fileUploaded, $model) { // fileUploaded = $_FILES["file"]
		$validextensions = array("jpeg", "jpg", "png");
		$temporary = explode(".", $fileUploaded["name"]);
		$file_extension = end($temporary) ;


		if ( ($fileUploaded["type"] !== "image/png" && $fileUploaded["type"] !== "image/jpg" && $fileUploaded["type"] !== "image/jpeg") && !in_array($file_extension, $validextensions) ) {
			return array('status'=>false, 'message'=>' solo se aceptan imagenes jpeg, jpg, png $file_extension');
		}

		if ($fileUploaded["size"] > 400000) { //approx. 400kb files can be uploade
			return array('status'=>false, 'message'=>' la imagen debe ser menor a 400kb ');
		}

		if ( file_exists( 'public' . DIRECTORY_SEPARATOR .'persona' . DIRECTORY_SEPARATOR . $fileUploaded[ 'name' ] ) ) {

			return array('status'=>false, 'message'=>'Existe una imagen con el mismo nombre, cambie el nombre de la imagen por favor');

		} else {

				$tempPath = $fileUploaded[ 'tmp_name' ];
		    $uploadPath = 'public' . DIRECTORY_SEPARATOR .'persona' . DIRECTORY_SEPARATOR . $fileUploaded[ 'name' ];

		    if (move_uploaded_file( $tempPath, $uploadPath )) { return array('status'=>true, 'message'=>"La imagen se subio exitosamente",'name'=> $fileUploaded[ 'name' ] );  }
		    else { return array('status'=>false, 'message'=>"Ocurrio un error, no se pudo subir la imagen, reintente");  }
		    
		}

	}


}

?>
