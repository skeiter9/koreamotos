<?php   

require_once 'cado.php';  

class Cargo extends Cado{

	private $table = 'cargo';

	public function rest($method,$table,$dataInput,$methods){

		$sql= "select c.*, a.nombre as area, count(ac.id_trabajador) as num_trabajadores  from cargo as c left join asignacion_cargo as ac on c.id_cargo=ac.id_cargo inner join area as a on c.id_area=a.id_area  group by c.nombre" ;

		//GET
		if ( $method === 'POST' && count($methods) === 0 ) {
			
			return $this->query( array('token'=> $dataInput['token'] ,'table'=> $this->table, 'crud'=> 'r' ,'indep'=> array( $this->table => array('sql'=>$sql )) )); 

		}

	}


}

?>
