<?php  

require_once 'cado.php';  

class Area extends Cado{

	private $table = 'area' ;

	public function rest($method,$table,$dataInput,$methods ){//get put post delete

		$sql="select a.id_area, a.nombre, a.estado, a.fecha_inicio_labores, a.descripcion , count(c.nombre) as num_cargos from area as a left join cargo as c on a.id_area=c.id_area group by a.nombre ";

		if ( $method === 'POST' && count($methods) === 0 ) { //get all items of this Model

			return $this->query( array('token'=> $dataInput['token'] , 'table'=> $this->table, 'crud'=> 'r' ,'indep'=> array( $this->table => array('sql'=>$sql  )) )); 

		}

		return array('status'=>false, 'message'=>"El método restufull no fue encontrado");
 
	}
}
?>

 
