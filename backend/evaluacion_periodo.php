<?php   

require_once 'cado.php';  

class Evaluacion_periodo extends Cado{

	private $table = 'evaluacion_periodo' ;

	public function rest($method, $table, $dataInput,$methods ){//get put post delete

		$sql= "select * from $this->table";
		$indep= array();
		$dep= array();

		if ( $method === 'POST' && count($methods) === 0 ) {
			return $this->query(array('token'=> $dataInput['token'] , 'table'=>$table,'crud'=>'r' ,'indep'=>array($this->table => array('sql'=> $sql) ) )); 
		}
		elseif ( $method === 'POST' && count($methods) === 1 && $methods[0] === 'new' ) {

			//return array('status'=>true, 'respond'=> $dataInput );

			$indep['evaluacion_periodo'] = array( 
				'sql'=>" insert into evaluacion_periodo ( mes, anio ) values ( ?, ? )",
				'array'=>array( date('n'), date('Y') ) 
			);


			//return array('status'=> true, 'data'=> $dataInput );
				$dep['evaluacion_campo'] = array( 
					'sql'=>" insert into evaluacion_campo ( id_evaluacion_periodo, id_campo, resultado ) values ( :id_evaluacion_periodo, :id_campo1, :resultado1  ), ( :id_evaluacion_periodo, :id_campo2, :resultado2  ), ( :id_evaluacion_periodo, :id_campo3, :resultado3  ), ( :id_evaluacion_periodo, :id_campo4, :resultado4  ) ",
					'array'=>array(
							':id_evaluacion_periodo'=> null, 
							':id_campo1'=> 1, ':resultado1'=> $dataInput['evaluacion_campo']['id_campo_1'] ,
							':id_campo2'=> 2, ':resultado2'=> $dataInput['evaluacion_campo']['id_campo_2'] ,
							':id_campo3'=> 3, ':resultado3'=> $dataInput['evaluacion_campo']['id_campo_3'] ,
							':id_campo4'=> 4, ':resultado4'=> $dataInput['evaluacion_campo']['id_campo_4']  
					)
				);
			
			return $this->query(array('token'=> $dataInput['token'] , 'table'=>$table,'crud'=>'r' ,'indep'=>$indep, 'dep'=>$dep )); 
			
		}


	}

}

?>
