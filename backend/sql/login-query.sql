select 
	x.cargo,x.id_cargo, x.fullname ,x.nombres, x.apellido_paterno,x.apellido_materno , x.id_persona, x.area,
  x.id_area, x.usuario, x.id_usuario, x.token
from ( 
  select 
    dt.cargo, dt.area as area, p.fullname,p.nombres,p.apellido_paterno,p.apellido_materno,
    dt.id_area,dt.id_cargo,p.id_persona , u.usuario, u.password ,u.id_usuario, u.token 
  from 
   	asignacion_cargo as ac  
    inner join ( 
      select ca.id_cargo, ca.nombre as cargo,ar.nombre as area,ar.id_area 
      from cargo as ca  
      inner join area as ar on ca.id_area=ar.id_area  
    ) as dt on ac.id_cargo=dt.id_cargo 
    	  
    inner join  (  
      select concat(pe.nombres,' ',pe.apellido_paterno,' ',pe.apellido_materno ) as fullname,      
        pe.nombres, pe.apellido_paterno, pe.apellido_materno   , t.id_trabajador,
        pe.id_persona 
      from persona as pe  
      inner join trabajador as t on pe.id_persona=t.id_persona  
    ) as p  on ac.id_trabajador=p.id_trabajador 

    inner join  usuario as u on ac.id_usuario=u.id_usuario 
) 

as x  where x.usuario=:usuario  and x.password=:password
