-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 01, 2014 at 09:42 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `korea`
--

-- --------------------------------------------------------

--
-- Table structure for table `area`
--

CREATE TABLE IF NOT EXISTS `area` (
  `id_area` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish2_ci NOT NULL,
  `fecha_inicio_labores` date NOT NULL,
  `estado` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_area`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `area`
--

INSERT INTO `area` (`id_area`, `nombre`, `descripcion`, `fecha_inicio_labores`, `estado`) VALUES
(1, 'TI', 'Área de TI', '2014-07-31', 1),
(2, 'gerencia general', 'gerencia general', '2000-06-16', 1),
(3, 'gerencia de Recursos Humano', 'gerencia de Recursos Humano', '2014-11-11', 1),
(4, 'ventas', 'Area de ventas', '2014-12-03', 1);

-- --------------------------------------------------------

--
-- Table structure for table `asignacion_cargo`
--

CREATE TABLE IF NOT EXISTS `asignacion_cargo` (
  `id_asignacion_cargo` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_inicio` date NOT NULL,
  `fecha_fin` date DEFAULT NULL,
  `id_cargo` int(11) NOT NULL,
  `id_trabajador` int(11) NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `id_pago` int(11) NOT NULL,
  PRIMARY KEY (`id_asignacion_cargo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `asignacion_cargo`
--

INSERT INTO `asignacion_cargo` (`id_asignacion_cargo`, `fecha_inicio`, `fecha_fin`, `id_cargo`, `id_trabajador`, `id_usuario`, `id_pago`) VALUES
(1, '2014-07-24', '2014-07-31', 1, 1, 1, 1),
(2, '2014-11-19', '2014-11-06', 2, 2, 2, 2),
(3, '2014-12-01', NULL, 3, 3, 3, 3),
(4, '2014-12-01', NULL, 4, 4, NULL, 4),
(5, '2014-12-02', NULL, 4, 5, NULL, 5),
(6, '2014-12-01', NULL, 4, 6, NULL, 6),
(7, '2014-12-02', NULL, 4, 7, NULL, 7),
(8, '2014-12-09', NULL, 4, 8, NULL, 8);

-- --------------------------------------------------------

--
-- Table structure for table `asignacion_menu`
--

CREATE TABLE IF NOT EXISTS `asignacion_menu` (
  `id_asignar_menu` int(11) NOT NULL AUTO_INCREMENT,
  `id_cargo` int(11) NOT NULL,
  `id_modulo` int(11) NOT NULL,
  `crud_c` tinyint(1) DEFAULT NULL,
  `crud_r` tinyint(1) DEFAULT NULL,
  `crud_u` tinyint(1) DEFAULT NULL,
  `crud_d` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_asignar_menu`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci AUTO_INCREMENT=56 ;

--
-- Dumping data for table `asignacion_menu`
--

INSERT INTO `asignacion_menu` (`id_asignar_menu`, `id_cargo`, `id_modulo`, `crud_c`, `crud_r`, `crud_u`, `crud_d`) VALUES
(1, 1, 1, 1, 1, 1, 1),
(2, 1, 2, 1, 1, 1, 1),
(3, 1, 3, 1, 1, 1, 1),
(4, 1, 4, 1, 1, 1, 1),
(5, 1, 5, 1, 1, 1, 1),
(6, 2, 1, 1, 1, 1, 1),
(7, 2, 2, 0, 0, 0, 0),
(8, 2, 3, 0, 0, 0, 0),
(9, 2, 4, 0, 0, 0, 0),
(10, 3, 5, 1, 1, 1, 1),
(11, 3, 1, 1, 1, 1, 1),
(12, 3, 2, 1, 1, 1, 1),
(13, 3, 3, 1, 1, 1, 1),
(14, 3, 4, 1, 1, 1, 1),
(15, 3, 5, 1, 1, 1, 1),
(51, 1, 6, 1, 1, 1, 1),
(52, 1, 7, 1, 1, 1, 1),
(53, 1, 8, 1, 1, 1, 1),
(54, 1, 9, 1, 1, 1, 1),
(55, 1, 10, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `campo`
--

CREATE TABLE IF NOT EXISTS `campo` (
  `id_campo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(150) COLLATE utf8_spanish2_ci NOT NULL,
  `tipo` int(11) NOT NULL,
  PRIMARY KEY (`id_campo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `campo`
--

INSERT INTO `campo` (`id_campo`, `nombre`, `tipo`) VALUES
(1, 'visitas realizadas por mes', 1),
(2, 'Nº ventas Deseado para el presente periodo', 1),
(3, 'Total de Trabajadores', 1),
(4, 'Total de Trabajadores rotados o retirados', 1),
(5, 'Ventas Realizadas este periodo', 2),
(6, 'Días trabajados este mes', 2),
(7, 'Días ausentes', 2);

-- --------------------------------------------------------

--
-- Table structure for table `cargo`
--

CREATE TABLE IF NOT EXISTS `cargo` (
  `id_cargo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish2_ci NOT NULL,
  `fecha_inicio_labores` date NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `id_area` int(11) NOT NULL,
  PRIMARY KEY (`id_cargo`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `cargo`
--

INSERT INTO `cargo` (`id_cargo`, `nombre`, `descripcion`, `fecha_inicio_labores`, `estado`, `id_area`) VALUES
(1, 'Administrador del sistema', 'es el super usuario, tiene permisos totales a todos los modulos, si cualquier usuario detecta algun error (Bug) , necesita una nueva función o quiere sugerir mejoras, debe contactarse con el usuario que tenga este tipo de cargo, este cargo está a cargo del equipo de Yetsu S.A.C', '2014-08-27', 1, 1),
(2, 'Gerente', 'Gerente', '2014-08-30', 1, 2),
(3, 'Gerente de R.R.H.H.', 'Gerente de recursos Humanos', '2014-08-22', 1, 3),
(4, 'vendedor', 'vendedor', '2014-12-10', 1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `evaluacion_campo`
--

CREATE TABLE IF NOT EXISTS `evaluacion_campo` (
  `id_evaluacion_campo` int(11) NOT NULL AUTO_INCREMENT,
  `id_evaluacion_periodo` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `id_campo` int(11) NOT NULL,
  `id_trabajador` int(11) DEFAULT NULL,
  `resultado` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id_evaluacion_campo`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=106 ;

--
-- Dumping data for table `evaluacion_campo`
--

INSERT INTO `evaluacion_campo` (`id_evaluacion_campo`, `id_evaluacion_periodo`, `id_campo`, `id_trabajador`, `resultado`) VALUES
(105, '6', 4, NULL, '15'),
(104, '6', 3, NULL, '14'),
(103, '6', 2, NULL, '13'),
(102, '6', 1, NULL, '12');

-- --------------------------------------------------------

--
-- Table structure for table `evaluacion_periodo`
--

CREATE TABLE IF NOT EXISTS `evaluacion_periodo` (
  `id_evaluacion_periodo` int(11) NOT NULL AUTO_INCREMENT,
  `mes` int(11) NOT NULL,
  `anio` int(11) NOT NULL,
  PRIMARY KEY (`id_evaluacion_periodo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `evaluacion_periodo`
--

INSERT INTO `evaluacion_periodo` (`id_evaluacion_periodo`, `mes`, `anio`) VALUES
(6, 12, 2014);

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE IF NOT EXISTS `item` (
  `idItem` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `descripcion` varchar(400) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`idItem`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`idItem`, `nombre`, `descripcion`) VALUES
(1, 'eficacia', 'Ventas realizadas al mespor el trabajador entre visitas realizadas al mess'),
(2, 'Resultado', 'Total de ventas Realizadas por mes'),
(3, 'Productividad', 'Total de ventas realizadas por mes entre Horas trabajadas por mes de cada trabajador'),
(4, 'Ausentismo', 'Dias Hombre ausentes entre dias trabajados'),
(5, 'Indicador de rotacion', 'Total de trabajadores rotados entre numero de trabajadores');

-- --------------------------------------------------------

--
-- Table structure for table `modulo`
--

CREATE TABLE IF NOT EXISTS `modulo` (
  `id_modulo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `nombreModel` varchar(25) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id_modulo`),
  UNIQUE KEY `nombre` (`nombre`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `modulo`
--

INSERT INTO `modulo` (`id_modulo`, `nombre`, `nombreModel`) VALUES
(1, 'vendedores', 'trabajador'),
(2, 'usuarios', 'usuario'),
(3, 'items', 'item'),
(4, 'evaluar', 'evaluacion'),
(5, 'reportes', 'reportes'),
(6, 'areas', 'area'),
(7, 'cargos', 'cargo'),
(8, 'campos de evaluación', 'campo'),
(9, 'Evaluar periodo de ventas', 'evaluacion_periodo'),
(10, 'evaluar campo', 'evaluacion_campo');

-- --------------------------------------------------------

--
-- Table structure for table `noplanilla`
--

CREATE TABLE IF NOT EXISTS `noplanilla` (
  `id_noplanilla` int(11) NOT NULL AUTO_INCREMENT,
  `sueldo` float NOT NULL,
  `id_pago` int(11) NOT NULL,
  PRIMARY KEY (`id_noplanilla`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `noplanilla`
--

INSERT INTO `noplanilla` (`id_noplanilla`, `sueldo`, `id_pago`) VALUES
(1, 1000, 0),
(5, 20000, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pago`
--

CREATE TABLE IF NOT EXISTS `pago` (
  `id_pago` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` int(11) NOT NULL,
  PRIMARY KEY (`id_pago`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `pago`
--

INSERT INTO `pago` (`id_pago`, `tipo`) VALUES
(1, 2),
(2, 2),
(3, 2),
(4, 2),
(5, 2),
(6, 2),
(7, 2),
(8, 2);

-- --------------------------------------------------------

--
-- Table structure for table `persona`
--

CREATE TABLE IF NOT EXISTS `persona` (
  `id_persona` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `photo` varchar(200) COLLATE utf8_spanish2_ci NOT NULL,
  `apellido_paterno` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `apellido_materno` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `dni` int(8) NOT NULL,
  `direccion` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `telefono` int(11) NOT NULL,
  `email` varchar(80) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id_persona`),
  UNIQUE KEY `dni` (`dni`),
  UNIQUE KEY `telefono` (`telefono`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `persona`
--

INSERT INTO `persona` (`id_persona`, `nombres`, `photo`, `apellido_paterno`, `apellido_materno`, `dni`, `direccion`, `fecha_nacimiento`, `telefono`, `email`) VALUES
(1, 'Percy', 'percy.jpg', 'Cubas', 'Vilchez', 47658943, 'Alfonso Ugarte 628', '1991-12-14', 995631274, 'percy323@gmail.com'),
(2, 'fidel', 'omar.jpg', 'galvez', 'bustamante', 32432432, 'JR amorarca 54', '1980-11-13', 998765436, 'fidel.987u@gmail.com'),
(3, 'Orlando', 'omar.jpg', 'galvez ', 'bustamante', 40765435, 'Los parques 456', '1985-11-04', 967854367, 'orlando_282@hotmail.com'),
(4, 'cesar', 'vendedor-hombre.jpg', 'julca', 'peréz', 42356789, 'Calle Conquista Nº 203 URB. LATINA - JLO', '1990-05-06', 965678542, 'jper_21@hotmail.com'),
(5, 'jorge', 'vendedor-hombre2.jpg', 'cajusol ', 'sanchez ', 43456789, 'san martin 675', '1987-11-04', 98765425, 'caj_sagitario98@hotmail.com'),
(6, 'gladis', 'vendedor-mujer.jpg', 'carrero ', 'nunes', 45678965, 'los olivares 987', '1985-11-03', 956785324, 'gladis_85_er@hotmail.com'),
(7, 'Marcos A.', 'vendedor-hombre3.jpg', 'lluncor', 'mestanza', 43567856, 'juan orbegoso 987 - Chiclayo', '1986-11-10', 42365426, 'mar45_34@hotmail.com'),
(8, 'agustin', 'vendedor-hombre4.jpg', 'ruiz ', 'agurto', 43567589, 'teniente pinglo 345', '1991-06-11', 98767890, 'sel_90@hotmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `planilla`
--

CREATE TABLE IF NOT EXISTS `planilla` (
  `id_planilla` int(11) NOT NULL AUTO_INCREMENT,
  `num_planilla` int(11) NOT NULL,
  `sueldo` float NOT NULL,
  `id_pago` int(11) NOT NULL,
  PRIMARY KEY (`id_planilla`),
  UNIQUE KEY `num_planilla` (`num_planilla`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=1002 ;

--
-- Dumping data for table `planilla`
--

INSERT INTO `planilla` (`id_planilla`, `num_planilla`, `sueldo`, `id_pago`) VALUES
(1001, 1001, 2000, 0);

-- --------------------------------------------------------

--
-- Table structure for table `trabajador`
--

CREATE TABLE IF NOT EXISTS `trabajador` (
  `id_trabajador` int(11) NOT NULL AUTO_INCREMENT,
  `estado` tinyint(1) NOT NULL,
  `fecha_contratacion` date NOT NULL,
  `fecha_despido` date DEFAULT NULL,
  `id_persona` int(11) NOT NULL,
  PRIMARY KEY (`id_trabajador`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `trabajador`
--

INSERT INTO `trabajador` (`id_trabajador`, `estado`, `fecha_contratacion`, `fecha_despido`, `id_persona`) VALUES
(1, 1, '2014-07-24', NULL, 1),
(2, 1, '2014-11-19', NULL, 2),
(3, 1, '2014-12-09', NULL, 3),
(4, 1, '2014-12-01', NULL, 4),
(5, 1, '2014-12-15', NULL, 5),
(6, 1, '2014-12-01', NULL, 6),
(7, 1, '2014-11-02', NULL, 7),
(8, 1, '2014-12-02', NULL, 8);

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `token` varchar(150) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`id_usuario`),
  UNIQUE KEY `usuario` (`usuario`),
  UNIQUE KEY `usuario_2` (`usuario`),
  UNIQUE KEY `token` (`token`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `usuario`, `password`, `token`) VALUES
(1, 'admin', '123456', 'aaaaa'),
(2, 'gerente', '123456', 'bbbbb'),
(3, 'gerenterrhh', '123456', 'ccccc');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
