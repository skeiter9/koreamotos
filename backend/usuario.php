<?php   

require_once 'cado.php';

class Usuario extends Cado{

	private $table = 'usuario' ;

	public function rest($method,$table,$dataInput,$methods){

		//GET
		if ( $method === 'GET' && count($methods)=== 1 && $methods[0]==='logout' ) {
			return $this->logout();
		}

		//POST
		elseif ( $method === 'POST' && count($methods)=== 1 && $methods[0]==='data' ) {
			return $this->data($dataInput['token']);
		}
		
		elseif ( $method === 'POST' && count($methods) === 1 && $methods[0]==='login' ) {
			return $this->login( $dataInput );
		}

	}
	
	// catch token
	public function login( $data ){

		if ( isset( $data['token'] ) ) {
			
			$usuario= array(
				'sql'=>"select x.token from ( select dt.cargo, dt.area as area, p.fullname,p.nombres,p.apellido_paterno,p.apellido_materno, dt.id_area,dt.id_cargo,p.id_persona , u.usuario, u.password ,u.id_usuario, u.token from asignacion_cargo as ac  inner join  ( select ca.id_cargo, ca.nombre as cargo,ar.nombre as area,ar.id_area from cargo as ca  inner join area as ar on ca.id_area=ar.id_area  ) as dt  on ac.id_cargo=dt.id_cargo inner join  (  select concat(pe.nombres,' ',pe.apellido_paterno,' ',pe.apellido_materno ) as fullname,      pe.nombres, pe.apellido_paterno, pe.apellido_materno   , t.id_trabajador,pe.id_persona from persona as pe  inner join trabajador as t on pe.id_persona=t.id_persona  )as p  on ac.id_trabajador=p.id_trabajador inner join  usuario as u on ac.id_usuario=u.id_usuario ) as x  where x.token= :token",
				'array'=>array( ':token' => $data['token'])
			);

		} else {

			$usuario= array(
				'sql'=>"select x.token from ( select dt.cargo, dt.area as area, p.fullname,p.nombres,p.apellido_paterno,p.apellido_materno, dt.id_area,dt.id_cargo,p.id_persona , u.usuario, u.password ,u.id_usuario, u.token from asignacion_cargo as ac  inner join  ( select ca.id_cargo, ca.nombre as cargo,ar.nombre as area,ar.id_area from cargo as ca  inner join area as ar on ca.id_area=ar.id_area  ) as dt  on ac.id_cargo=dt.id_cargo inner join  (  select concat(pe.nombres,' ',pe.apellido_paterno,' ',pe.apellido_materno ) as fullname,      pe.nombres, pe.apellido_paterno, pe.apellido_materno   , t.id_trabajador,pe.id_persona from persona as pe  inner join trabajador as t on pe.id_persona=t.id_persona  )as p  on ac.id_trabajador=p.id_trabajador inner join  usuario as u on ac.id_usuario=u.id_usuario ) as x  where x.usuario=:usuario  and x.password=:password",
				'array'=>array(":usuario"=> $data['usuario'],':password'=> $data['password'])
			);

		}

		$result = $this->query( array('login'=> true, 'indep'=>array('usuario'=>$usuario) ) ); 

		if ($result['status'] && count($result['respond']['usuario']['respond'])>0 ) {

			return array('status'=>true, 'token'=> $result['respond']['usuario']['respond'][0]->token );
		
		}elseif ($result['status'] && count($result['respond']['usuario']['respond'])===0) {
			return array('status'=>false, 'message'=>'El usuario y/o la contraseña son incorrectos') ;
		}else{
			return $result;// error on datebase
		}
	}	

	public function data($token){

		return $this->query( array( 'token'=> $token, 'userData'=> true ) ); 

	}
	
	public function logout(){
		if (isset($_SESSION['user'])) {
			unset( $_SESSION['user'] );
			return array('status'=>true,'message'=>'Ha cerrado sesion' );
		}else{
			return array('status'=>false,'message'=>'Debe loguearse' );
		}
	}

	
}
?>