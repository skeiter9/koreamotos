<?php  
require_once 'helpers.php';
require_once 'cado.php';    
class Persona extends Helpers{
	
	function newRow($data){
		$checkDni= $this->checkDniPersona( $data['persona_dni'] , 8 );
		if ( $checkDni['band'] ) {
			$result = parent::newRow($data);
			if ($result['query']) {
				print_r($result);
				return array('band'=>true,'message'=>"Se registro la persona correctamente ");
			} else {
				return array('band'=>false,'message'=>"No se pudo registrar la persona intentelo mas tarde");
			}
			return $result;
		} else {
			return $checkDni;
		}
		
	}

	function checkDniPersona($num,$len){
		$respond = parent::checkInteger($num,$len);
		if ($respond) {
			$respond = parent::checkFieldUnique(array('table'=>'persona','field'=>'dni','fieldExpress'=>$num));
			if ($respond['band']) {
				return array('band'=>true,'message'=>"El Dni es Correcto ");
			} else {
				return array('band'=>false,'message'=>"El Dni ya ha sido registrado a: {$respond['message']['nombres']} {$respond['message']['apellido_paterno']} {$respond['message']['apellido_materno']}  ");
			}
		} else {
			return array('band'=>false,'message'=>"El Dni es incorrecto ");
		}
			
	}




	public function rest($method,$table,$methodSpecial,$dataInput){//get put post delete
		

		if( isset($methodEspecial) && is_numeric($methodEspecial) ){
			
		}

		$data=array();

		switch ($method) {
			case 'GET':

				if ( count($dataInput) > 1 ) {
					if ( isset($dataInput['dni'] ) ) {
						$db= new Cado;
						$data= array('sql'=>'select * from persona where dni=?','array'=>array($dataInput['dni']) );
						$bd = $db->multiQuery( array('indep'=> array('agregado'=>$data ) ) ) ;
						return ($bd['status'])?array('status'=>true,'message'=>"Selección exitosa",'respond'=>$bd['respond']['agregado']['respond']  ):array('status'=>false,'message'=>"(POST) {$bd['message']}  ");
					}else{
						return array('false'=>false, 'message'=>"el parametro no es seleccionable");
					}
				}else{
					$data=array('accion'=>'select','columns'=>'p.* ,t.id_trabajador,t.estado ,t.fecha_contratacion,t.fecha_despido, c.cargo, ac.id_cargo, c.cargo_estado as cargo_estado ,a.nombre as area ,a.id_area, a.estado as area_estado','table'=>'trabajador as t inner join persona as p on t.id_persona=p.id_persona inner join asignacion_cargo as ac on t.id_trabajador=ac.id_trabajador inner join ( select ca.id_cargo,ca.nombre as cargo, ca.estado as cargo_estado, ca.id_area from cargo as ca ) as c on ac.id_cargo = c.id_cargo inner join area as a on c.id_area=a.id_area');
					$bd= $this->crud($data);
					return ( $bd['band'] )? array('status'=>true,'message'=>"Selección exitosa",'respond'=>$bd['query']->fetchAll(PDO::FETCH_CLASS) ) : array('status'=>false,'message'=>"{$bd['message']} ");
				}

				break;
			case 'POST':

				$persona=array('sql'=>" insert into persona (nombres, apellido_paterno, apellido_materno, dni, direccion, fecha_nacimiento, telefono, email) values (:nombres, :apellido_paterno, :apellido_materno, :dni, :direccion, :fecha_nacimiento, :telefono, :email)",'array'=>array(':nombres'=>$dataInput['persona']['nombres'] , ':apellido_paterno'=>$dataInput['persona']['apellido_paterno'] , ':apellido_materno'=>$dataInput['persona']['apellido_materno'] , ':dni'=>$dataInput['persona']['dni'] , ':direccion'=>$dataInput['persona']['direccion'] , ':fecha_nacimiento'=>$dataInput['persona']['fecha_nacimiento'] , ':telefono'=>$dataInput['persona']['telefono'] , ':email'=>$dataInput['persona']['email'] ) );
				$usuario=array('sql'=>"insert into usuario (usuario, password ) values ( :usuario, :password )",'array'=>array(':usuario'=>$dataInput['usuario']['usuario'] , ':password'=>$dataInput['usuario']['password'] ) );
				
					$pagoTable="";
					$pago=array();
					if ( $dataInput['trabajador']['pago']=== '1' ) { //planilla
						$pagoTable='planilla';
						$pago=array('sql'=>" insert into planilla (num_planilla, sueldo) values (num_planilla, :sueldo)",'array'=>array('num_planilla'=>$dataInput['planilla']['num_planilla'] , ':sueldo'=>$dataInput['planilla']['sueldo'] ) );
						}elseif ( $dataInput['trabajador']['pago']=== '2' ) { //Recibo por honorarios
						$pagoTable='noplanilla';
						$pago=array('sql'=>" insert into noplanilla (sueldo) values (:sueldo)",'array'=>array(':sueldo'=>$dataInput['noplanilla']['sueldo'] ) );
					}

					$trabajador = array('sql'=> " insert into Trabajador ( estado, fecha_contratacion, fecha_despido, id_planilla, id_noplanilla, id_persona ) values ( :estado,:fecha_contratacion,:fecha_despido,:id_planilla,:id_noplanilla,:id_persona )",'arrayPre'=>$dataInput['trabajador'] );
					$asignacion_cargo = array('sql'=> " insert into Asignacion_cargo (fecha_inicio,fecha_fin,id_cargo,id_trabajador,id_usuario) values (:fecha_inicio, :fecha_fin, :id_cargo, :id_trabajador, :id_usuario)",'arrayPre'=> $dataInput['asignacion_cargo'] );
				
				// database conection

				$data=array('persona'=>$persona, $pagoTable => $pago,'usuario'=>$usuario );
				$dataDependient= array( 'trabajador'=> $trabajador,'asignacion_cargo'=> $asignacion_cargo );
				$bd= new Cado;
				$result= $bd->multiQuery( array('indep'=>$data, 'dep'=>$dataDependient ) );
				return $result;
			break;
			default:
				return array('status'=>false,'message'=>"No se encontro el método solicitado");
				break;
		}
			
	}










}
 
?>