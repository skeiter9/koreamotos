<?php

class Cado{

	private $host="localhost";
	private $user="root";
	private $password="";
	private $engine="mysql";
	private $port="";
	private $dbname="korea";

	private $pdo;

	public function __construct () {
		try {
		  $db=new PDO("mysql:dbname=$this->dbname;charset=UTF8;host=$this->host",$this->user,$this->password,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8" ,PDO::MYSQL_ATTR_INIT_COMMAND => "SET CHARACTER SET utf8"));
		  $this->pdo = $db;
		} catch (PDOException $e) {
		  return array('status'=> false,"message"=> 'Falló la conexión: '. $e->getMessage() );
		}
	}


	protected function query( $data ) {

		//return $data;
		// Login 
		if ( isset($data['login'])  ) { return $this->execQuery( $data );	 }
		//return $data;
		if ( !isset($data['token'])  ) { return array('status'=>false,'message'=>'No se envio el token'); }

		if ( $data['token'] === null  ) { return array('status'=>false,'message'=>'el token no puede ser nulo '); }

		$tokenStatus = $this->checkToken( $data['token'] );

		if ( !isset($tokenStatus['status']) ) {
			
			return array('status'=>false,'message'=>'No se envio el token o no está logueado ');

		}

		// only get user data
		if ( isset($data['userData'])  ) {

			return $tokenStatus;

		}
		
		if ( !in_array( $data['crud'] ,  array('c', 'r', 'u', 'd') ) ) {
			return array('status'=>false,'message'=>"Solo estan permitidas opereaciones CRUD" );
		}

		for ($i=0; $i < count( $tokenStatus['respond']['asignacion_menu']['respond'] ) ; $i++) { 

			if ( $tokenStatus['respond']['asignacion_menu']['respond'][$i]->nombreModel === $data['table'] ) {

				foreach (  $tokenStatus['respond']['asignacion_menu']['respond'][$i] as $key => $value) {

					if ( $key === "crud_{$data['crud']}" ) {

						if ( $value === '1' ) {
									
							$result = $this->execQuery( $data );
							$result['respond']['usuario']= $tokenStatus['respond']['usuario'] ;	
							$result['respond']['asignacion_menu']= $tokenStatus['respond']['asignacion_menu'] ;	

							return $result;

						}else {

							$crud_message = ( $data['crud'] === 'c' ) ? ' crear items de ': '';
							$crud_message = ( $data['crud'] === 'r' ) ? ' ver el listado de ': '';
							$crud_message = ( $data['crud'] === 'u' ) ? ' actualizar items de ': '';
							$crud_message = ( $data['crud'] === 'd' ) ? ' eliminar items de ': '';

							return array('status'=>false,'message'=>"No tiene permisos para $crud_message ".$data['table'] );
						}

					}

				}

			} 

		}

		return array('status'=>false,'message'=>"El módulo solicitado no existe ",'data'=>$data);

	}

	private function checkToken ( $token ) {

		$usuario= array(
			'sql'=>"select x.cargo,x.id_cargo, x.fullname ,x.nombres, x.photo, x.apellido_paterno,x.apellido_materno , x.id_persona, x.area, x.id_area, x.usuario, x.id_usuario, x.token from ( select dt.cargo, dt.area as area, p.fullname,p.nombres, p.photo, p.apellido_paterno,p.apellido_materno, dt.id_area,dt.id_cargo,p.id_persona , u.usuario, u.password ,u.id_usuario, u.token from asignacion_cargo as ac  inner join  ( select ca.id_cargo, ca.nombre as cargo,ar.nombre as area,ar.id_area from cargo as ca  inner join area as ar on ca.id_area=ar.id_area  ) as dt  on ac.id_cargo=dt.id_cargo inner join  (  select concat(pe.nombres,' ',pe.apellido_paterno,' ',pe.apellido_materno ) as fullname, pe.photo, pe.nombres, pe.apellido_paterno, pe.apellido_materno   , t.id_trabajador,pe.id_persona from persona as pe  inner join trabajador as t on pe.id_persona=t.id_persona  )as p  on ac.id_trabajador=p.id_trabajador inner join  usuario as u on ac.id_usuario=u.id_usuario ) as x  where x.token= :token",
			'array'=>array( ':token' => $token)
		);
	
		$asignacion_menu= array(
			'sql'=>"select m.nombre, m.nombreModel,am.crud_c,am.crud_r,am.crud_u, am.crud_d from asignacion_menu as am  inner join modulo as m on am.id_modulo=m.id_modulo where id_cargo = :id_cargo",
			'array'=>array(":id_cargo"=> null )
		);

		return $this->execQuery( array( 'indep'=>array('usuario'=>$usuario), 'dep'=> array('asignacion_menu'=>$asignacion_menu) ) );

	}

	private function execQuery ( $data ) {

		$result= array();
		$lastIds=array();

		$this->pdo->beginTransaction();

		if ( isset($data['indep']) ) {

			foreach ($data['indep'] as $key => $value) {

				$value['array'] = ( isset( $value['array'] ) ) ? $value['array'] : array() ;

				$db = $this->pdo->prepare( $value['sql'] );
				$band = $db->execute( $value['array'] ) ;

				if ( !$band ) {
					$this->pdo->rollBack();
					return array('status'=>false,'message'=>  $db->errorInfo()[2] ." ( $key ) " ) ;
				}
													
				$result[$key]= array('status'=>true,'respond'=> $db->fetchAll(PDO::FETCH_CLASS) );

				
													 
				if ( is_array( $result[$key]['respond'] ) && count( $result[$key]['respond'] ) > 0 ) {

					foreach ( $result[$key]['respond'][0] as $keyQuery => $valueQuery ) { 

						if (strpos($keyQuery,'id_') !== false) {  $lastIds[$keyQuery] = $valueQuery; }

					}

				} 
				
				if ( $this->pdo->lastInsertId() !== '0' ) { $lastIds["id_$key"]= $this->pdo->lastInsertId() ; }// only if insert sentence before

				$db->closeCursor();
													
			}

		}
		//return $lastIds;
		if ( isset($data['dep']) ) {

			foreach ($data['dep'] as $key => $value) {

				//check if the dependencie is in the lastIds arrays(indep)
				foreach ($lastIds as $keyT => $valueT) {

					if ( array_key_exists(":$keyT", $value['array'] )  && $value['array'][":$keyT"] === null ) {

						$value['array'][":$keyT"] = $valueT;

					}

				}

				$db = $this->pdo->prepare( $value['sql'] );
				$band = $db->execute( $value['array'] ) ;

				if ( !$band ) { 
					$this->pdo->rollBack();
					return $result[$key] = array('status'=>false,'message'=>  $db->errorInfo()[2] ." ( $key )  " , 'lastIds'=> $lastIds ) ; 
				}

				$result[$key]= array('status'=>true,'respond'=> $db->fetchAll(PDO::FETCH_CLASS) );

				if ( is_array( $result[$key]['respond'] ) && count( $result[$key]['respond'] ) > 0 ) {
					foreach ( $result[$key]['respond'][0] as $keyQuery => $valueQuery ) { 

						if (strpos($keyQuery,'id_') !== false) {
								$lastIds[$keyQuery] = $valueQuery;
						}

					} 
				}

				if ( $this->pdo->lastInsertId() !== '0' ) { $lastIds["id_$key"]= $this->pdo->lastInsertId() ; }// only if insert sentence before

				$db->closeCursor();
								
			}

		}

		$this->pdo->commit();

		return  array('status'=>true, 'respond'=>$result ) ;
	}
	/********* functions *****/
	protected function form($data){  
		$query = (isset($data['id']))?$this->simpleQuery(array('table'=>"{$data['table']}",'crud'=>'r','sql'=>"select * from {$data['table']} where id_{$data['table']}=?",'array'=>array($data['id']) ))  : $this->simpleQuery(array('table'=>"{$data['table']}",'crud'=>'r','sql'=>"select * from {$data['table']}" ))   ;  

 		if ($query['status']) {

				$columns= array();

				for ($i = 0; $i < $query['query']->columnCount() ; $i ++) {
				  $meta = $query['query']->getColumnMeta($i); 
				  if ( strpos($meta['name'], 'id_') === false) { 

				  	$type="";
				  	$required=false;

				  	switch ($meta['native_type']) {
				    	case 'LONG':
				    		$type="number";
				    		break;
				    	case 'FLOAT':
				    		$type="number";
				    		break;
				    	case 'DATE':
				    		$type="date";
				    		break;
				    	case 'VAR_STRING':
				    		$type="text";
				    		break;
				    	case 'SHORT':
				    		$type="checkbox";
				    		break;
				    	case 'TINY':
				    		$type="switch";
				    		break;
				    	case 'BLOB':
				    		$type="textarea";
				    		break;	
				    	default:
				    		$type=$meta['native_type'];
				    		break;
				  	}

						$required = ( isset($meta['flags'][0]) && $meta['flags'][0]==="not_null" )?true:false;

				  	$columns[$i]=  array('name'=> $meta['name'], 'type'=>$type, 'required'=>$required,'max'=>$meta['len'],'table'=>$meta['table'] );

				  	if ( isset($data['id']) ) { $columns[$i]['value'] = $query['respond'][0]->$meta['name']; }

					}
					if (  strpos($meta['name'], 'id_') !== false && $meta['name']!=="id_{$data['table']}" ) {

						$required = ( isset($meta['flags'][0]) && $meta['flags'][0]==="not_null" )?true:false;
						$tableRequired=substr($meta['name'], 3); //_Get the table required for select
						$queryOptions = $this->simpleQuery(array('table'=>$tableRequired,'crud'=>'r',  'method'=>'form', 'sql'=>"select * from ". $tableRequired  )); 
						if (!$queryOptions['status']) { return $queryOptions; }
						$columns[$i]=  array('name'=> $meta['name'], 'type'=>'select','options'=>$queryOptions['respond'], 'required'=>$required,'max'=>$meta['len'],'table'=>$meta['table'] );

						if ( isset($data['id']) ) { $columns[$i]['value'] = $query['respond'][0]->$meta['name']; }
					}
				}
				return array('status'=>true,'respond'=>$columns);
		}else{
			return array('status'=>false,'message'=>"Ocurrio un error en la base de datos, intentelo nuevamente ({$query['message']})" );
		} 
	}//return all fields of table with all features
}
