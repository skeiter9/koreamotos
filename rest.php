<?php 

session_start();

require_once 'backend/restfull.php'; 
try {
	/*
		$_REQUEST['request'] :uri
		$_SERVER['REQUEST_METHOD'] = request type get/post/put/delete
	*/
		//check if the request method is PUT or DELETE

		$uri = $_REQUEST['request']; //get the uri retrieved from url

		//return $_SERVER['REQUEST_METHOD'];

		switch($_SERVER['REQUEST_METHOD']){
			case 'PUT':
				parse_str(file_get_contents('php://input'), $_PUT);
				$_REQUEST=$_PUT;
				$_REQUEST['request']=$uri;
			break;

			case 'DELETE':
				parse_str(file_get_contents('php://input'), $_DELETE);
				$_REQUEST=$_DELETE;
				$_REQUEST['request']=$uri;
			break;
		}

		unset($_REQUEST['request']);



	$api = new Restfull($uri, $_SERVER['REQUEST_METHOD'], $_REQUEST);
	
	echo json_encode( $api->respond() );
	//echo json_encode( $_REQUEST);
	//echo json_encode( $_POST);

} catch (Exception $e) {
	echo json_encode(array('status' => false, "message"=>" No se pudo conectar con la API ". $e->getMessage()));
}
